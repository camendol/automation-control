from logging import Handler
import requests

class MattermostHandler(Handler):
    """
    Interface to mattermost channel notifications through logging.

    :param url: mattermost incoming webhook urllib3
    """
    def __init__(self, url: str):
        Handler.__init__(self)
        self.url = url
        self.levels = {
            None : '#4287f5',
            'INFO' : '#4287f5',
            'success' : '#1be800',
            'WARNING' : '#ff9100',
            'ERROR' : '#cf0000',
            'CRITICAL' : '#000000'
        }

    def emit(self, record):
        """
        Send notification to mattermost.

        :param record: LogRecord object.
        """
        msg = self.format(record)
        
        data = {
            'attachments' : [
                {
                    'color': self.levels[record.levelname],
                    'text' : msg
                }
            ]
        }

        requests.post(self.url, json = data)

