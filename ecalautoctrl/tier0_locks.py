'''
This module provide an interface to the T0 monitoring pages that
do not support HTTPS GET queries and needs to be parsed manually.
Locks are implemented to wait for full processing of Repack/Express/Prompt
'''

import requests
from typing import List, Dict, Optional, Union, Tuple
from .TaskHandlers import LockBase

class T0ProcDatasetLock(LockBase):
    """
    This class provides a lock mechanism based on each run processing
    status at T0. Given a dataset name the lock method checks for
    processing being completed for the given dataset primary dataset and
    the specified processing stage (Repack, Express, PromptReco).

    :param dataset: dataset name. Either full (including wildcards) or pd only (/EGamma, /EGamma/Run2022*/RAW-RECO, etc).
    :param stage: Repack, Express, PromptReco. If omitted check all.
    """

    def __init__(self, dataset: str, stage: Optional[str]=None, **kwargs):
        super().__init__()

        self.t0url = 'https://dmytro.web.cern.ch/dmytro/cmsprodmon/tier0_details.php'
        self.valid_stages = ['Repack', 'Express', 'PromptReco']

        self.stage = None
        # check validity of specified stage
        if stage:
            self.stage = [s for s in self.valid_stages if s.casefold() == stage.casefold()]
        # in case of invalid stage or omitted stage keep all
        if not self.stage:
            self.stage = self.valid_stages
            self.log.warning(f'T0ProcDatasetLock - {stage} is not a valid T0 stage. Specify one among Repack, Express, PromptReco')

        # keep only the primary dataset name
        self.pd = dataset.split('/')[1] if '/' in dataset else dataset
        self.log.info(f'T0ProcDatasetLock - will check status for PD {self.pd}')
        
    def __str__(self):
        return 'T0ProcDatasetLock'
        
    def lock(self, runs: List[Dict]) -> List[bool]:
        """
        Check if processing of specified stage+dataset is marked completed by T0.

        :params runs: a list of runs in the :class:`~ecalautoctrl.RunCtrl` format.
        :return: the corresponding lock status. 
        """

        locks = len(runs)*[True]
        for i,r in enumerate(runs):
            info = {}
            data = requests.get(f'{self.t0url}?run={r["run_number"]}')
            if data.ok:
                for l in data.text.split('\n'):
                    # keep only lines containing the pd and any of the stages
                    if 'addRow' in l and self.pd.casefold() in l.casefold() and any([s in l for s in self.stage]):
                        t = l.split(',')
                        info[t[0][t[0].find('>'):t[0].rfind('<')]] = t[1].strip("'")=='completed'
                # consistency check
                #assert(len(info) == len(self.stage))

                # release the lock (i.e. return False) if processing has been completed for all.
                locks[i] = not all(info.values())
            else:
                # hold processing if data not available
                locks[i] = True
                self.log.warning(f'T0ProcDatasetLock - cannot find info for run {r["run_number"]} on T0. Processing in on hold.')
                            
        return locks
